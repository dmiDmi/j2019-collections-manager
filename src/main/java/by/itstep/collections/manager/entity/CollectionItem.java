package by.itstep.collections.manager.entity;

import lombok.*;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
public class CollectionItem {

    private Long id;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Collection collection;

    private String name;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Tag> tag;
}
