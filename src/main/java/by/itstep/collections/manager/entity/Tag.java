package by.itstep.collections.manager.entity;

import lombok.*;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
public class Tag {

    private Long id;

    private String name;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Collection> collections;
}
