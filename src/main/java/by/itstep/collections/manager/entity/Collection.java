package by.itstep.collections.manager.entity;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class Collection {

    private Long id;

    private String name;

    private int title;

    private String description;

    private String imageUrl;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<CollectionItem> items;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Comment> comments;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;


}
