package by.itstep.collections.manager.entity;

import lombok.*;

import java.sql.Date;


@Data
@Builder
@NoArgsConstructor
public class Comment {

    private Long id;

    private String message;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;// Тот кто его оставил

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Collection collection;// К чему относится

    private Date createdAt;
}
